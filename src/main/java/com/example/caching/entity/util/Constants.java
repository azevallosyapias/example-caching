package com.example.caching.entity.util;

public class Constants {

	public static enum STATUS {
		ACTIVO("A", "Activo"), ELIMINADO("X", "Deshabilitado");

		private String codigo;
		private String message;

		private STATUS(String codigo, String message) {
			this.codigo = codigo;
			this.message = message;
		}

		public String getCodigo() {
			return codigo;
		}

		public String getMessage() {
			return message;
		}

	}

	public enum OBSERV {
	    CREATE, UPDATE, DELETE;
	}
	
}
