package com.example.caching.service;

import java.util.List;

import com.example.caching.entity.Pet;

public interface IPetService {

	public List<Pet> findAllPets();
	public Pet createPet(Pet pet);
	public Pet updatePet(Pet pet);
	public Pet findByID(Long id);
}
