package com.example.caching.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.caching.dao.PetsDao;
import com.example.caching.dto.TPet;
import com.example.caching.entity.Pet;
import com.example.caching.entity.util.Constants;

@Service
public class PetServiceImpl implements IPetService {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	PetsDao petsDao;

	@Cacheable(value = "pets")
	@Override
	public List<Pet> findAllPets() {
		log.info("Entrando al metodo para consultar BD.....");
		List<TPet> lst = petsDao.findByPetStatu(Constants.STATUS.ACTIVO.getCodigo());
		List<Pet> pets = new ArrayList<Pet>();
		lst.stream().forEach(x -> pets.add(setPet(x)));

		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return pets;
	}

	@CacheEvict(value = "pets", allEntries = true)
	@Override
	public Pet createPet(Pet pet) {
		TPet petS = new TPet();
		petS.setPetStatu(Constants.STATUS.ACTIVO.getCodigo());
		petS.setPetBirthDate(pet.getBirthdate());
		petS.setPetName(pet.getName());
		// llenando datos de auditoria
		petS.setAuditFecope(null);
		petS.setAuditFecre(LocalDateTime.now());
		petS.setAuditIP("127.0.0.0");
		petS.setAuditObs(Constants.OBSERV.CREATE);
		petS.setAuditUser("admin");

		petS = petsDao.save(petS);
		return setPet(petS);
	}

	@CachePut(value="pet", key="#pet.id")
	@CacheEvict(value = "pets", allEntries = true)
	@Override
	public Pet updatePet(Pet pet) {

		TPet petS = new TPet();
		petS.setPetIident(pet.getId());
		petS.setPetStatu(Constants.STATUS.ACTIVO.getCodigo());
		petS.setPetBirthDate(pet.getBirthdate());
		petS.setPetName(pet.getName());
		// llenando datos de auditoria
		petS.setAuditFecope(null);
		petS.setAuditFecre(LocalDateTime.now());
		petS.setAuditIP("127.0.0.0");
		petS.setAuditObs(Constants.OBSERV.UPDATE);
		petS.setAuditUser("admin");
		petS = petsDao.save(petS);
		return setPet(petS);
	}

	private Pet setPet(TPet p) {
		Pet pet = new Pet();
		pet.setBirthdate(p.getPetBirthDate());
		pet.setId(p.getPetIident());
		pet.setName(p.getPetName());
		pet.setStatus(p.getPetStatu());

		return pet;
	}

	@Cacheable(value="pet", key="#id")
	@Override
	public Pet findByID(Long id) {
		Optional<TPet> oPet = petsDao.findById(id);
		if(oPet.isPresent()) {
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return setPet(oPet.get());
		}else {
			return null;
		}
		
	}
}
