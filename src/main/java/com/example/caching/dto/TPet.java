package com.example.caching.dto;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.example.caching.entity.util.Constants.OBSERV;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "t_pet")
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TPet {

	@Id
	@SequenceGenerator(name = "sequence_pet", sequenceName = "sq_pets", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_pet")
	@Column(name = "pet_iident")
	@Setter 
	@Getter
	private Long petIident;
	
	@Column(name = "pet_name")
	@Setter 
	@Getter
	private String petName;
	
	@Column(name = "pet_birthdate")
	@Setter 
	@Getter
	private LocalDateTime petBirthDate;
	
	@Column(name = "pet_cstatu")
	@Setter 
	@Getter
	private String petStatu;
	
	@Column(name = "audit_dfecope")
	@Setter 
	@Getter
	private LocalDateTime auditFecope;
	
	@Column(name = "audit_dfecre")
	@Setter 
	@Getter
	private LocalDateTime auditFecre;
	
	@Column(name = "audit_vipad")
	@Setter 
	@Getter
	private String auditIP;
	
	@Column(name = "audit_vobs")
	@Setter 
	@Getter
	@Enumerated(EnumType.STRING)
	private OBSERV auditObs;
	
	@Column(name = "audit_vuser")
	@Setter 
	@Getter
	private String auditUser;
	
}
