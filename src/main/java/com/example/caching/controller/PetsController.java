package com.example.caching.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.caching.entity.Pet;
import com.example.caching.service.IPetService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/pets")
@Api(tags = "pets")
public class PetsController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	IPetService petService;

	@ApiOperation(value = "Get all pets", notes = "Get all pets")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK") })
	@GetMapping(path = "/getpets", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public List<Pet> listAllPets() {
		log.info("get all pets");
		return petService.findAllPets();
	}

	@ApiOperation(value = " ", notes = "Get one Pet")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Invalid pet id"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Pet not found") })
	@GetMapping(path = "/getpetById/{petId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public Pet getUserByID(@PathVariable("petId") Long petId) {
		log.info("GetMapping - value: /getpetById/" + petId);
		return petService.findByID(petId);
	}

	@ApiOperation(value = "Create Pet", notes = "")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "CREATED"),
			@ApiResponse(code = HttpServletResponse.SC_METHOD_NOT_ALLOWED, message = "Invalid input") })
	@PostMapping(path = "/createPet", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public Pet savePet(@RequestBody Pet pet) {
		log.info("create pet: " + pet.toString());
		return petService.createPet(pet);
	}

	@ApiOperation(value = "", notes = "")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Invalid pet id"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Pet not found") })
	@PutMapping(path = "/updatePetById/{petId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public Pet putUser(@PathVariable("petId") Long petId, @RequestBody Pet pet) {
		log.info("PutMapping - value: /updatePetById/" + petId);
		pet.setId(petId);
		return petService.updatePet(pet);
	}

}
