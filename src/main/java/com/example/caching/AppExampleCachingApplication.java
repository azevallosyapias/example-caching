package com.example.caching;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class AppExampleCachingApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppExampleCachingApplication.class, args);
	}

}
