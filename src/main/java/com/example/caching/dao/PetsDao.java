package com.example.caching.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.caching.dto.TPet;

public interface PetsDao extends CrudRepository<TPet, Long> {

	List<TPet> findByPetStatu(String estado);
}
