-- Sequence: public.sq_pets

-- DROP SEQUENCE public.sq_pets;

CREATE SEQUENCE public.sq_pets
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.sq_pets
  OWNER TO postgres;



-- Table: public.t_pet

-- DROP TABLE public.t_pet;

CREATE TABLE public.t_pet
(
  pet_iident bigint NOT NULL, -- Id de tabla
  pet_name character varying(255), -- Id de la mascota
  pet_birthdate timestamp without time zone, -- Fecha de nacimiento de la mascota
  pet_cstatu character(1), -- Estado del registro
  audit_dfecope timestamp without time zone, -- Campo de auditoria - fecha de actualizacion del registro
  audit_dfecre timestamp without time zone, -- Campo de auditoria - fecha de creacion del registro
  audit_vipad character varying(255), -- Campo de auditoria - ip origen de la peticion
  audit_vobs character varying(255), -- Campo de auditoria - observacion
  audit_vuser character varying(255), -- Campo de auditoria - usuario
  CONSTRAINT pk_pet_id PRIMARY KEY (pet_iident)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.t_pet
  OWNER TO postgres;
COMMENT ON COLUMN public.t_pet.pet_iident IS 'Id de tabla';
COMMENT ON COLUMN public.t_pet.pet_name IS 'Id de la mascota';
COMMENT ON COLUMN public.t_pet.pet_birthdate IS 'Fecha de nacimiento de la mascota';
COMMENT ON COLUMN public.t_pet.pet_cstatu IS 'Estado del registro';
COMMENT ON COLUMN public.t_pet.audit_dfecope IS 'Campo de auditoria - fecha de actualizacion del registro';
COMMENT ON COLUMN public.t_pet.audit_dfecre IS 'Campo de auditoria - fecha de creacion del registro';
COMMENT ON COLUMN public.t_pet.audit_vipad IS 'Campo de auditoria - ip origen de la peticion';
COMMENT ON COLUMN public.t_pet.audit_vobs IS 'Campo de auditoria - observacion ';
COMMENT ON COLUMN public.t_pet.audit_vuser IS 'Campo de auditoria - usuario';

